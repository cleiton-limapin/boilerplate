{% comment %}  
### Criando o projeto com boilerplate:

```bash
django-admin startproject -e py,md,txt --template=https://gitlab.com/cleiton-limapin/boilerplate/-/archive/master/boilerplate-master.zip <project_name> [path_dir]
```  
{% endcomment %}
# {{project_name}}

Descrição do projeto

### Install (TODO)

##### Criar o arquivo `.env` na raiz do projeto
```bash
cp contrib/env_sample.txt .env
```

##### dev
```bash
pip install -r requirements/dev.txt
./manage.py migrate
./manage.py runserver
```

##### test
```bash
pip install -r requirements/test.txt
./manage.py test -n
```

##### prod
```bash
pip install -r requirements/prod.txt
./manage.py migrate
```