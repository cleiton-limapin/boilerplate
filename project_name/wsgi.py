"""
WSGI config for {{project_name}} project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.1/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application

project_name = '{{project_name}}'

os.environ.setdefault('DJANGO_SETTINGS_MODULE', f'{project_name}.settings.prod')

application = get_wsgi_application()
