from dj_database_url import parse

from .base import *

SECRET_KEY = 'shhh,is-secret'

DEBUG = True

DATABASES = {
    'default': parse('sqlite:///' + os.path.join(BASE_DIR, 'database-dev.sqlite3'))
}

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
EMAIL_HOST = config('EMAIL_HOST', default='')
EMAIL_PORT = config('EMAIL_PORT', default=587, cast=int)
EMAIL_HOST_USER = config('EMAIL_HOST_USER', default='')
EMAIL_HOST_PASSWORD = config('EMAIL_HOST_PASSWORD', default='')
EMAIL_USE_TLS = config('EMAIL_USE_TLS', default=True, cast=bool)
