#!/usr/bin/env python
import os
import sys

from decouple import config

project_name = '{{project_name}}'

if __name__ == '__main__':
    settings_module = config('DJANGO_SETTINGS_MODULE', default=None)

    if len(sys.argv) > 1 and sys.argv[1] == 'test':
        print("Ignoring config('DJANGO_SETTINGS_MODULE') because it's test. "
              f"Using '{project_name}.settings.test'")

        os.environ.setdefault('DJANGO_SETTINGS_MODULE', f'{project_name}.settings.test')
    else:
        if settings_module is None:
            print('Error: no DJANGO_SETTINGS_MODULE found. Will NOT start '
                  'devserver. Remember to create .env file at project root. '
                  'Check README for more info.')
            sys.exit(1)

        os.environ.setdefault('DJANGO_SETTINGS_MODULE', settings_module)

    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc

    execute_from_command_line(sys.argv)
